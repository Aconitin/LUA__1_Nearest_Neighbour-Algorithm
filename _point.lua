_point = {

	canvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight()),
	
	instances = {},
	
	create = function(self, x, y, class)
		local class = class or false
		if self.instances and self.instances[x] and self.instances[x][y] then
			return nil
		end
		if not class then
			class = self:getNearestPointClass(x, y)
		end
		local func = function()
			love.graphics.setPointSize(1)
			love.graphics.setPointStyle("rough")
			love.graphics.setColor(gVars.classes[class].r, gVars.classes[class].g, gVars.classes[class].b, 255)
			love.graphics.point(x, y)
		end
		self.canvas:renderTo(func)
		if not self.instances[x] then
			self.instances[x] = {}
		end
		self.instances[x][y] = class
		return true
	end,
	
	getNearestClassInRectangle = function(self, x, y, w, h)
		local retClass = nil
		local dmx = x - w
		if dmx < 0 then dmx = 0 end
		local dpx = x + w
		if dpx > love.graphics.getWidth() then dpx = love.graphics.getWidth() end
		local dmy = y - h
		if dmy < 0 then dmy = 0 end
		local dpy = y + h
		if dpy > love.graphics.getHeight() then dpy = love.graphics.getHeight() end
		for i=dmx,dpx,1 do
			for j=dmy,dpy,1 do
				if self.instances[i] and self.instances[i][j] then
					retClass = self.instances[i][j]
				end
				if retClass then break end
			end
			if retClass then break end
		end
		return retClass
	end,
	
	getNearestPointClass = function(self, x, y)
		local retClass = nil
		local rectW = 1
		local rectH = 1
		while retClass == nil do
			retClass = self:getNearestClassInRectangle(x, y, rectW, rectH)
			rectW = rectW + 1
			rectH = rectH + 1
		end
		return retClass
	end,
}