function love.conf(t)
    t.title = "ONNA"
    t.identity = "ONNA"
    t.version = "0.9.0"
    t.console = true
    t.window.width = 1800
    t.window.height = 900
    t.window.fullscreen = false
    t.window.vsync = false --> Don't change this!
    t.window.fsaa = 0
	t.window.borderless = false
	t.window.resizable = false
	t.window.centered = true
    t.modules.joystick = true
    t.modules.audio = true
    t.modules.keyboard = true
    t.modules.event = true
    t.modules.image = true
    t.modules.graphics = true
    t.modules.timer = true
    t.modules.mouse = true
    t.modules.sound = true
    t.modules.physics = true
end